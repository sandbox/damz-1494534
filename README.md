
Stageable

`Stageable` is a generic framework for moving (ie. staging) various types of objects between Drupal sites.

The core module supports staging the following object types:

* Core version (read-only, used as a dependency)
* Enabled module (read-only, used as a dependency)
* Variables

In addition, the Stageable Entity module allow staging of:

* Entity type definitions (read-only, used as a dependency)
* Entity bundle definitions (read-only for the base implementation, used as a dependency)
  * Node types as a special-cases of bundle definition
* The content of entities themselves, of any type (including core entities: nodes, comments, users, taxonomy vocabulary, taxonomy terms, files)
* Fields and field instances

In addition, the Stageable CTools module allow staging of:

* Any type of CTools exportable objects (including Views, Panels, etc.)

