<?php

class FriendlySerialize {
  static function serialize($variable) {
    // Use PHP to do the primary serialization.
    $serialized = serialize($variable);
    $state = array(
      'input' => $serialized,
      'pos' => 0,
      'values' => array(),
    );
    return FriendlySerialize::doSerialize($state, '');
  }

  static function unserialize($variable) {
    $state = array(
      'object count' => array(),
    );
    $output = FriendlySerialize::doUnserialize($variable, '', $state);
    // Use PHP to do the primary deserialization.
    return unserialize($output);
  }

  static protected function doSerialize(&$state, $path = NULL) {
    $type = $state['input']{$state['pos']++};
    if ($type !== 'N') {
      FriendlySerialize::expect(':', $state);
    }

    if (isset($path) && $type != 'R') {
      // For reference types, don't allocate an object slot.
      $value = &$state['values'][$path];
      $state['values_idx'][] = $path;
    }

    switch ($type) {
      case 'N':
        $value = NULL;
        FriendlySerialize::expect(';', $state);
        break;
      // Booleans.
      case 'b':
        $value = (bool) FriendlySerialize::readUntil(';', $state);
        break;
      // Integers.
      case 'i':
        $value = (int) FriendlySerialize::readUntil(';', $state);
        break;
      // Decimals.
      case 'd':
        $value = (float) FriendlySerialize::readUntil(';', $state);
        break;
      // Strings.
      case 's':
        $value = FriendlySerialize::readString($state);
        FriendlySerialize::expect(';', $state);
        break;
      // Arrays.
      // Objects.
      case 'O':
        $class_name = FriendlySerialize::readString($state);
        FriendlySerialize::expect(':', $state);
      case 'a':
        $length = (int) FriendlySerialize::readUntil(':', $state);
        FriendlySerialize::expect('{', $state);
        $value = array();
        if ($type == 'O') {
          $value["\0\0class"] = $class_name;
        }

        for ($i = 0; $i < $length; $i++) {
          $key = FriendlySerialize::doSerialize($state, NULL);
          $value[$key] = FriendlySerialize::doSerialize($state, $path . '.' . $key);
        }
        FriendlySerialize::expect('}', $state);
        break;
      // References to objects.
      case 'r':
      // References to variables.
      case 'R':
        $idx = (int) FriendlySerialize::readUntil(';', $state);
        $value = "\0\0" . $type . ":" . $state['values_idx'][$idx - 1];
        break;
      default:
        throw new FriendlySerializeParseException($type, "valid type", $state);
    }
    return $value;
  }

  protected static function doUnserialize($value, $path = NULL, &$state) {
    $output = '';

    if (isset($path) && !(is_string($value) && substr($value, 0, 4) == "\0\0R:")) {
      $state['object count']['::' . $path] = count($state['object count']) + 1;
    }

    if (is_null($value)) {
      $output .= 'N;';
    }
    if (is_bool($value)) {
      $output .= 'b:' . ($value ? 1 : 0) . ';';
    }
    else if (is_int($value)) {
      $output .= 'i:' . $value . ';';
    }
    else if (is_float($value)) {
      $output .= 'd:' . sprintf('%f', $value) . ';';
    }
    else if (is_string($value)) {
      if (substr($value, 0, 4) == "\0\0R:" || substr($value, 0, 4) == "\0\0r:") {
        $output .= $value{2} . ':' . $state['object count']['::' . substr($value, 4)] . ';';
      }
      else {
        $output .= 's:' . strlen($value) . ':"' . $value . '";';
      }
    }
    else if (is_array($value)) {
      if (isset($value["\0\0class"])) {
        // It's actually an object.
        $output .= 'O:' . strlen($value["\0\0class"]) . ':"' . $value["\0\0class"] . '":';
        unset($value["\0\0class"]);
      }
      else {
        // It's a boring array.
        $output .= 'a:';
      }
      // Length.
      $output .= count($value) . ':';
      $output .= '{';
      foreach ($value as $key => $value) {
        // Key.
        $output .= FriendlySerialize::doUnserialize($key, NULL, $state);
        // Value.
        $output .= FriendlySerialize::doUnserialize($value, $path . '.' . $key, $state);
      }
      $output .= '}';
    }

    return $output;
  }

  static protected function expect($char, &$state) {
    if ($state['input']{$state['pos']} !== $char) {
      throw new FriendlySerializeParseException($state['input']{$state['pos']}, $char, $state);
    }
    $state['pos']++;
  }

  static protected function readString(&$state) {
    $length = (int) FriendlySerialize::readUntil(':', $state);
    FriendlySerialize::expect('"', $state);
    $value = substr($state['input'], $state['pos'], $length);
    $state['pos'] += $length;
    FriendlySerialize::expect('"', $state);
    return $value;
  }

  static protected function readUntil($char, &$state) {
    $end = strpos($state['input'], $char, $state['pos']);
    if ($end === FALSE) {
      FriendlySerialize::throwEndOfStreamError();
    }
    $value = substr($state['input'], $state['pos'], $end - $state['pos']);
    $state['pos'] = $end + 1;
    return $value;
  }

}

class FriendlySerializeParseException extends Exception {
  function __construct($character, $expected, $state) {
    echo substr($state['input'], $state['pos'] - 15, 30);
    parent::__construct('Unexpected character "' . $character . '" at position ' . $state['pos'] . ' expected ' . $expected);
  }
}
