<?php
require('../HumaneSerialize.php');

// Simple types.
$tests[] = NULL;
$tests[] = FALSE;
$tests[] = TRUE;
$tests[] = 1;
$tests[] = 123;
$tests[] = 1.123;
$tests[] = -1.40;
$tests[] = "1";
$tests[] = "123";

// Simple arrays.
$tests[] = array(123, 456, 789);
$tests[] = array(123, 456, array(1, 2, 3));

// Associative arrays.
$tests[] = array("key" => "value");
$tests[] = array("key" => array(1, 2, 3));

// Anonymous objects.
$tests[] = (object) array("key" => "value");

// Complex objects.
class A {
  public $public;
  private $private;
  protected $protected;
  function __construct($public, $private, $protected) {
    $this->public = $public;
    $this->private = $private;
    $this->protected = $protected;
  }
}
$tests[] = new A("public value", "private value", "protected value");

// References to variables.
$a = array(1);
$a[] = &$a[0];
$tests[] = $a;

// References to objects.
$a = (object) array();
$a->self = $a;
$a->b = "toto";
$a->b_ref = &$a->b;
$a->c = "titi";
$a->c_ref = &$a->c;
$a->d = "titi";
$a->d_ref = &$a->d;
$a->e = $a->self;
$a->f = "titi";
$a->f_ref = &$a->f;
$a->g = array(1, 2, 3, array(4, 5, 6));
$a->g_3_2_ref = &$a->g[3][2];
$tests[] = $a;

// Out-of-order references.
$a = (object) array();
$a->a = NULL;
$a->b = "a";
$a->a = &$a->b;
$tests[] = $a;

foreach ($tests as $test) {
  $round_trip = FriendlySerialize::unserialize(FriendlySerialize::serialize($test));
  if (serialize($round_trip) != serialize($test)) {
    echo "\n\nInput ===\n"; print_r($test);
    echo "\nSerialized ===\n"; echo(serialize($test) . "\n");
    echo "\nRound trip ===\n"; echo(serialize($round_trip) . "\n");
  }
}
