<?php

class PrettyJSON {

  /**
   * Returns the JSON representation of a value with pretty-printing.
   *
   * @param $value
   *  The value being encoded. Can be any type except a resource. This function
   *  only works with UTF-8 encoded data.
   * @param $options
   *  Bitmask consisting of JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS,
   *  JSON_NUMERIC_CHECK, JSON_PRETTY_PRINT, JSON_UNESCAPED_SLASHES,
   *  JSON_FORCE_OBJECT, JSON_UNESCAPED_UNICODE.
   * @return
   *  Returns a JSON encoded string on success.
   */
  public static function encode($content, $options = 0) {
    if (version_compare(PHP_VERSION, '5.4.0', '>=')) {
      return json_encode($content, $options | JSON_PRETTY_PRINT);
    }
    else {
      return PrettyJSON::encodeRecursive($content, $options);
    }
  }

  /**
   * Utility function for json_encode_pretty().
   */
  protected static function encodeRecursive($content, $options, $indent = '') {
    if (is_array($content) || is_object($content)) {
      // A PHP array can be represented as a JSON array if it contains only
      // consecutive numeric keys.
      $is_json_array = !($options & JSON_FORCE_OBJECT == JSON_FORCE_OBJECT) && is_array($content) && (range(0, count($content) - 1) === array_keys($content));
      $separators = $is_json_array ? '[]' : '{}';

      $content = (array) $content;
      if (empty($content)) {
        // In case of an empty object, just output a short-form JSON array or object.
        return $separators;
      }
      else {
        // Else, output each key with the proper indent prefix.
        $output = array();
        foreach ((array) $content as $key => $value) {
          // Only output the key if we are rendering an object.
          $prefix = !$is_json_array ? json_encode($key, $options) . ': ' : '';
          $output[] = $prefix . PrettyJSON::encodeRecursive($value, $options, $indent . '  '); 
        }
        return $separators{0} . "\n" . $indent . '  ' . implode(',' . "\n" . $indent . '  ', $output) . "\n" . $indent . $separators{1};
      }
    }
    else {
      // Fall-back to just json_encode() in every other cases.
      return json_encode($content, $options);
    }
  }

}
