--TEST--
JSON serialization with pretty-printing.
--INI--
error_reporting = E_ALL
--FILE--
<?php
require('PrettyJSON.php');

function output($variable) {
  echo PrettyJSON::encode($variable) . "\n";
}
?>
Integers:
<?php output(1) ?>
<?php output(-1) ?>
<?php output(12345) ?>

Floats:
<?php output(1.3333) ?>
<?php output(-1.3333) ?>
<?php output(12345.0) ?>

Arrays:
<?php output(array()) ?>
<?php output(array(1, 2, 3)) ?>
<?php output(array(1, 2, 3, "a")) ?>
<?php output(array(1, 2, array(4, 5, 6))) ?>

Associative arrays:
<?php output(array("a" => "b", "c" => "d")) ?>
<?php output(array(0, 1, 2, "a" => 1)) ?>

Objects:
<?php output((object) array()) ?>
<?php output((object) array("a" => "b", "c" => "d")) ?>

Mixed types:
<?php output((object) array("a" => "b", "c" => array(1, 2, "3", "d" => 1.333))) ?>

--EXPECT--
Integers:
1
-1
12345

Floats:
1.3333
-1.3333
12345

Arrays:
{}
[
  1,
  2,
  3
]
[
  1,
  2,
  3,
  "a"
]
[
  1,
  2,
  [
    4,
    5,
    6
  ]
]

Associative arrays:
{
  "a": "b",
  "c": "d"
}
{
  0: 0,
  1: 1,
  2: 2,
  "a": 1
}

Objects:
{}
{
  "a": "b",
  "c": "d"
}

Mixed types:
{
  "a": "b",
  "c": {
    0: 1,
    1: 2,
    2: "3",
    "d": 1.333
  }
}
