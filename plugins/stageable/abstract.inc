<?php

interface StageableObject {
  public function __construct(array $info);

  /**
   * Create an exportable representation of a set of object ids.
   */
  public function export(array $object_ids);

  /**
   * Perform the actions necessary to ensure that a set of object definitions are applied on the current site.
   */
  public function ensure(array $object_definitions);

  /**
   * Check if an object exists on the current site.
   */
  public function exists($object_id);

  /**
   * Load a set of object IDs from the current site.
   */
  public function load(array $object_ids);
}

abstract class StageableObjectAbstract implements StageableObject {
  protected $pluginInfo;

  public function __construct(array $info) {
    return $this->pluginInfo = $info;
  }

  public function ensure(array $object_definitions) {

  }

  /**
   * (Internal use) Process the dependencies between objects in a set of definitions.
   */
  protected function processObjectDependencies(array $object_definitions) {
    // Extract dependencies to compute the graph.
    $graph = array();
    $all_dependencies = array();
    foreach ($object_definitions as $object_id => $object_definition) {
      $graph[$object_id]['edges'] = array();
      if (isset($object_definition['dependencies'])) {
        foreach ($object_definition['dependencies'] as $dependency) {
          $all_dependencies[] = $dependency;
          $graph[$object_id]['edges'][$dependency] = 1;
        }
      }
    }

    // Load the IDs of dependencies that already exist in the database.
    $existing_objects = $this->load($all_dependencies);

    foreach ($all_dependencies as $object_id) {
      if (!isset($existing_objects[$object_id]) && !isset($object_definitions[$object_id])) {
        // Each dependency must either exist in the database or be present
        // in the incoming $object_definitions.
        throw new Exception(t('Dependent entity @object_id does not exist.', array('@object_id' => $object_id)));
      }
    }

    // Process the dependency graph.
    require_once DRUPAL_ROOT . '/includes/graph.inc';
    drupal_depth_first_search($graph);
    uasort($graph, 'drupal_sort_weight');

    return $graph;
  }

  /**
   * (Internal use) Group a list of object ids by a sub-group.
   */
  protected function groupObjectIds(array $object_ids) {
    $output = array();
    foreach ($object_ids as $object_id) {
      list($schema, $type, $name) = explode(':', $object_id, 3);
      $output[$type][$name] = $object_id;
    }
    return $output;
  }
}

abstract class StageableObjectAbstractReadonly extends StageableObjectAbstract {
  public function export(array $object_ids) {
    throw new StageableReadOnlyException();
  }

  public function ensure(array $object_definitions) {
    throw new StageableReadOnlyException();
  }

  public function load(array $object_definitions) {
    throw new StageableReadOnlyException();
  }
}

class StageableBroken extends StageableObjectAbstract {
  public function export(array $object_ids) {
    return array();
  }

  public function ensure(array $object_definitions) {
    return FALSE;
  }

  public function exists($object_id) {
    return FALSE;
  }

  public function load(array $object_definitions) {
    return FALSE;
  }
}
