<?php
$plugin = array(
  'title' => t('Drupal Core'),
  'description' => t('Checks the version of Drupal Core.'),
  'read only' => TRUE,
  'handler' => 'StageableDrupalCore',
  'schemes' => array('core'),
);

class StageableDrupalCore extends StageableObjectAbstractReadonly {
  public function exists($object_id) {
    return TRUE;
  }
}
