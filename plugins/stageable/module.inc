<?php
$plugin = array(
  'title' => t('Module dependency'),
  'description' => t('Checks that a module is enabled.'),
  'read only' => TRUE,
  'handler' => 'StageableModule',
  'schemes' => array('module'),
);

class StageableModule extends StageableObjectAbstractReadonly {
  public function exists($object_id) {
    list(, $module) = explode(':', $object_id, 2);
    return module_exists($module);
  }
}
