<?php

/**
 * Returns a list of entities exported by a given module, optionally filtered by entity type.
 */
function stageable_exported_entities($module, $entity_type = NULL) {
  if (!module_exists($module)) {
    throw new Exception('Module does not exist.');
  }

  $entity_types = isset($entity_type) ? array($entity_type) : array_keys(entity_get_info());
  $module_path = drupal_get_path('module', $module);
  $entities = array();
  foreach ($entity_types as $current_entity_type) {
    if (is_dir($module_path . '/' . $current_entity_type)) {
      $iterator = new DirectoryIterator();
      foreach ($iterator as $file) {
        if (!$file->isFile()) {
          continue;
        }
        $filename = $file->getFilename();
        if (substr($filename, -7) == '.export') {
          $entities[$current_entity_type][] = substr($filename, 0, -7);
        }
      }
    }
  }

  return isset($entity_type) ? (isset($entities[$current_entity_type]) ? $entities[$current_entity_type] : array()) : $entities;
}

/**
 * Export an entity to a module.
 */
function stageable_export_entities($module, $entity_type, array $entity_ids) {

  $storage = new StageableStorageModule('module:' . $module);

  $entities = entity_load($entity_type, $entity_ids);
  $entity_info = entity_get_info($entity_type);
  foreach ($entities as $entity_id => $entity) {
    list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);

    // Re-map properties.
    $entity = clone $entity;
    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    try {
      _stageable_properties_remap($wrapper);
    }
    catch (Exception $e) {
      echo (string) $e;
    }

    // Build a stable representation of the entity.


    $storage->store('entity:' . $entity_type . ':' . $entity_id, $output);
  }
}

function _stageable_properties_remap($wrapper, $recurse = TRUE) {

}



function stageable_revert_entities($module, $entity_type, array $entity_ids) {
  
}
