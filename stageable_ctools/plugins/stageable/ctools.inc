<?php

$plugin = array(
  'title' => t('CTools Exportable'),
  'description' => t('Stage CTools Exportable objects.'),
  'handler' => 'StageableCToolsExportable',
  'schemes' => array('ctools'),
);

class StageableCToolsExportable extends StageableObjectAbstract {
  public function load(array $object_ids) {
    
  }

  public function export(array $object_ids) {
    ctools_include('export');
    $output = array();
    foreach ($this->groupObjectIds($object_ids) as $table => $objects) {
      foreach ($objects as $name => $object_id) {
        $output[$object_id] = array(
          'object' => FriendlySerialize::serialize(ctools_export_crud_load($table, $name)),
          'dependencies' => array(),
        );
      }
    }
    return $output;
  }

  public function ensure(array $object_definitions) {
    // Nothing to do.
  }

  public function exists($object_id) {
    ctools_include('export');
    list(, $table, $name) = explode(':', $object_id, 3);
    return ctools_export_crud_load($table, $name) !== NULL;
  }
}
