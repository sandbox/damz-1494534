<?php
$plugin = array(
  'title' => t('Entity bundle dependency'),
  'description' => t('Checks that a bundle is defined for a given an entity type.'),
  'read only' => TRUE,
  'handler' => 'StageableEntityBundle',
  'schemes' => array('entity-bundle'),
);

class StageableEntityBundle extends StageableObjectAbstractReadonly {
  public function exists($object_id) {
    list(, $entity_type, $bundle) = explode(':', $object_id, 3);
    $entity_info = entity_get_info($entity_type);
    return isset($entity_info['bundles'][$bundle]);
  }
}
