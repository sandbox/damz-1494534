<?php
$plugin = array(
  'title' => t('Entity type'),
  'description' => t('Checks that an entity type .'),
  'read only' => TRUE,
  'handler' => 'StageableEntityType',
  'schemes' => array('entity-type'),
);

class StageableEntityType extends StageableObjectAbstractReadonly {
  public function exists($object_id) {
    list(, $entity_type) = explode(':', $object_id, 2);
    $entity_info = entity_get_info($entity_type);
    return isset($entity_info);
  }
}
