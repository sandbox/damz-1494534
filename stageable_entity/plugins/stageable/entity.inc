<?php
$plugin = array(
  'title' => t('Entity'),
  'description' => t('Import/export entities, respecting the mapping of entities between themselves.'),
  'handler' => 'StageableObjectEntity',
  'schemes' => array('entity'),
);

class StageableObjectEntity {
  public function export($object_ids) {
    $output = array();

    // Extract entity types and UUIDs.
    foreach ($this->loadIDsFromObjectIds($object_ids) as $entity_type => $ids) {
      $entities = entity_load($entity_type, $ids);
      foreach ($entities as $entity_id => $entity) {
        // Re-map properties.
        $entity = clone $entity;
        $dependencies = $this->processEntityForExport($entity_type, $entity);

        // Build the representation of the entity.
        $output['entity:' . $entity_type . ':' . $entity->uuid] = $this->buildExport($entity_type, $entity, $dependencies);
      }
    }
    return $output;
  }

  public function ensure($object_definitions) {
    // Load the IDs of existing objects.
    $existing_objects = $this->loadIDsFromObjectIds(array_keys($object_definitions), TRUE);

    foreach ($object_definitions as $object_id => $object_definition) {
      list(, $entity_type, $uuid) = explode(':', $object_id, 3);
      $entity_info = entity_get_info($entity_type);

      // Load the IDs of the dependencies.
      $entity_dependencies = array();
      foreach ($object_definition['dependencies'] as $dependency_id) {
        list($object_type, ) = explode(':', $dependency_id, 2);
        if ($object_type == 'entity') {
          $entity_dependencies[] = $dependency_id;
        }
      }
      $dependencies_ids = $this->loadIDsFromObjectIds($entity_dependencies, TRUE);

      if (isset($existing_objects[$object_id])) {
        $entity = entity_load_single($entity_type, $existing_objects[$object_id]);
        foreach ($this->processEntityForImport($object_definition['content'], $dependencies_ids) as $key => $value) {
          $entity->$key = $value;
        }
      }
      else {
        $entity = (object) $this->processEntityForImport($object_definition['content'], $dependencies_ids);
      }

      // Save the new entity id.
      entity_save($entity_type, $entity);
    }
  }

  /**
   * (Internal) Process the entity for export.
   */
  protected function processEntityForExport($entity_type, $entity) {
    $dependencies = array();
    $entity_info = entity_get_info($entity_type);
    list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);

    if (!empty($entity_info['base table'])) {
      $schema = drupal_get_schema($entity_info['base table']);
      if (!empty($schema['foreign keys'])) {
        $dependencies += $this->mapUUIDFromForeignKeys($schema, $entity);
      }
    }

    // Do the same for fields.
    foreach (field_info_instances($entity_type, $bundle) as $instance) {
      if (!isset($entity->{$instance['field_name']})) {
        continue;
      }

      $field_info = field_info_field($instance['field_name']);
      if (!empty($field_info['foreign keys'])) {
        foreach ($entity->{$instance['field_name']} as $langcode => $items) {
          foreach ($items as $delta => $item) {
            $item = (object) $item;
            $dependencies += $this->mapUUIDFromForeignKeys($field_info, $item);
            $entity->{$instance['field_name']}[$langcode][$delta] = (array) $item;
          }
        }
      }
    }

    return array_keys($dependencies);
  }

  protected function entityGetTypeByTable($table) {
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if (isset($entity_info['base table']) && $entity_info['base table'] == $table) {
        return $entity_type;
      }
    }
  }

  protected function mapUUIDFromForeignKeys($schema, &$data) {
    $dependencies = array();
    foreach ($schema['foreign keys'] as $foreign_key) {
      $table = $foreign_key['table'];
      if (count($foreign_key['columns']) == 1 && $foreign_entity_type = $this->entityGetTypeByTable($table)) {
        $destination_column = reset($foreign_key['columns']);
        $source_column = key($foreign_key['columns']);
        if (isset($data->{$source_column})) {
          $foreign_entity = entity_load_single($foreign_entity_type, $data->{$source_column});

          // The foreign entity doesn't support UUIDs.
          if (empty($foreign_entity->uuid)) {
            throw new Exception(t("Foreign entity @entity_type doesn't support UUIDs (@entity).", array('@entity_type' => $foreign_entity_type, '@entity' => '<pre>' . print_r($foreign_entity, TRUE) . '</pre>')));
          }

          $foreign_object_id = 'entity:' . $foreign_entity_type . ':' . $foreign_entity->uuid;
          $dependencies[$foreign_object_id] = TRUE;
          $data->{$source_column} = "\0" . $foreign_object_id;
        }
      }
    }
    return $dependencies;
  }

  protected function processEntityForImport(array $object_definition, array $uuids_map) {
    foreach ($object_definition as &$value) {
      if (is_array($value)) {
        $value = $this->processEntityForImport($value, $uuids_map);
      }
      else {
        if (is_string($value) && substr($value, 0, 1) == "\0") {
          $value = $uuids_map[substr($value, 1)];
        }
      }
    }
    return $object_definition;
  }

  protected function loadIDsFromObjectIds(array $object_ids, $flat = FALSE) {
    $entities = array();
    foreach ($object_ids as $object_id) {
      list(, $entity_type, $uuid) = explode(':', $object_id, 3);
      if (!StageableObjectEntity::exportableEntity($entity_type)) {
        throw new Exception(t('Entity type @entity_type cannot be exported.', array('@entity_type' => $entity_type)));
      }

      $entities[$entity_type][$object_id] = $uuid;
    }

    $output = array();
    foreach ($entities as $entity_type => $uuids) {
      $ids = entity_get_id_by_uuid($entity_type, $uuids);
      foreach ($uuids as $object_id => $uuid) {
        if (isset($ids[$uuid])) {
          if ($flat) {
            $output[$object_id] = $ids[$uuid];
          }
          else {
            $output[$entity_type][$object_id] = $ids[$uuid];
          }
        }
      }
    }
    return $output;
  }

  /**
   * (Internal) Build a merge-friendly representation of an entity.
   */
  protected function buildExport($entity_type, $entity, array $dependencies = array()) {
    $output = array(
      'content' => array(),
      'dependencies' => array(),
    );
    list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);
    $entity_info = entity_get_info($entity_type);

    // Output the main keys of the entity first.
    $keys = array();
    if (!empty($entity_info['entity keys']['uuid'])) {
      $keys[$entity_info['entity keys']['uuid']] = TRUE;
    }
    if (!empty($entity_info['entity keys']['bundle'])) {
      $keys[$entity_info['entity keys']['bundle']] = TRUE;
    }
    // Then the other properties.
    $properties = array();
    if (isset($entity_info['schema_fields_sql']['base table'])) {
      $properties += array_flip($entity_info['schema_fields_sql']['base table']);
    }
    if (isset($entity_info['schema_fields_sql']['revision table'])) {
      $properties += array_flip($entity_info['schema_fields_sql']['revision table']);
    }
    ksort($properties);
    $keys += $properties;

    // We don't export the entity id and revision id.
    if (!empty($entity_info['entity keys']['id'])) {
      unset($keys[$entity_info['entity keys']['id']]);
    }
    if (!empty($entity_info['entity keys']['revision'])) {
      unset($keys[$entity_info['entity keys']['revision']]);
    }
    if (!empty($entity_info['entity keys']['revision uuid'])) {
      unset($keys[$entity_info['entity keys']['revision uuid']]);
    }

    // Process the properties.
    foreach ($keys as $key => $dummy) {
      $output['content'][$key] = isset($entity->$key) ? $entity->$key : NULL;
    }

    // Then process the fields, in alphabetical order.
    $instances = field_info_instances($entity_type, $bundle);
    ksort($instances);
    foreach ($instances as $field_name => $instance) {
      $field = field_info_field($field_name);

      // Iterate through the languages, LANGUAGE_NONE first, then by alphabetic
      // order.
      $languages = array_flip(field_available_languages($entity_type, $field));
      ksort($languages);
      if (isset($languages[LANGUAGE_NONE])) {
        $languages = array(LANGUAGE_NONE => 0) + $languages;
      }
      foreach ($languages as $language => $dummy) {
        if (!isset($entity->{$field_name}[$language])) {
          continue;
        }
        foreach ($entity->{$field_name}[$language] as $delta => $item) {
          // Save only the schema keys of the field, in the order of their columns.
          foreach ($field['columns'] as $column_name => $column_info) {
            $output['content'][$field_name][$language][$delta][$column_name] = isset($item[$column_name]) ? $item[$column_name] : NULL;
          }
        }
      }
    }

    // Process the dependencies.
    $output['dependencies'][] = 'entity-type:' . $entity_type;
    if (isset($bundle)) {
      $output['dependencies'][] = 'bundle:' . $entity_type . ':' . $bundle;
    }
    // Add field-based dependencies.
    foreach ($instances as $field_name => $instance) {
      $output['dependencies'][] = 'field:' . $field_name;
      $output['dependencies'][] = 'field-instance:' . $entity_type . ':' . $bundle . ':' . $field_name;
    }
    // Add related-objects dependencies.
    $output['dependencies'] = array_merge($output['dependencies'], $dependencies);

    return $output;
  }

  public static function exportableEntity($entity_type) {
    $info = entity_get_info($entity_type);
    return isset($info['uuid']) && $info['uuid'] == TRUE && !empty($info['entity keys']['uuid']);
  }
}

/**
 * Special-purpose EntityValueWrapper that does not perform any validation.
 */
class StageableNoValidationEntityValueWrapper extends EntityValueWrapper {
  public function validate($value) {
    return TRUE;
  }
}
