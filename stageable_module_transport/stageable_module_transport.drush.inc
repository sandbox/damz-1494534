<?php

/**
 * @file
 * Drush integration for the stageable module transport.
 */

/**
 * Implements hook_drush_command().
 */
function stageable_module_transport_drush_command() {
  $items = array();

  $items['stageable-module-transport-add'] = array(
    'description' => 'Add an object to a module.',
    'drupal dependencies' => array('stageable'),
    'arguments' => array(
      'module' => 'Module to add to.',
      'entities' => 'List of object to add.',
    ),
    'aliases' => array('sa'),
    ''
  );
  $items['stageable-update'] = array(
    'description' => 'Update all entities added to a module.',
    'arguments' => array(
      'module' => 'A space delimited list of modules.',
    ),
    'drupal dependencies' => array('stageable'),
    'aliases' => array('su'),
  );

  return $items;
}

function drush_stageable_module_transport_add() {
  $args = func_get_args();

  if (count($args) <= 1) {
    return drush_set_error(dt('Usage: drush stageable-add [module] [entities...].'));
  }

  $module = array_shift($args);
  if (!module_exists($module)) {
    return drush_set_error(dt('Module !module does not exists or is not enabled.', array('!module' => $module)));
  }

  // Initialize the transport.
  $transport = new StageableModuleTransport($module);

  $object_controllers = array();
  $object_ids = array();
  foreach ($args as $object_id) {
    $controller = stageable_get_stageable_object($object_id);
    if ($controller instanceof StageableObjectBroken) {
      return drush_set_error(dt('Invalid object specification "!object".', array('!object' => $object_id)));
    }
    $object_controllers[get_class($controller)] = $controller;
    $object_ids[get_class($controller)][] = $object_id;
  }

  foreach ($object_ids as $class => $object_ids) {
    $controller = $object_controllers[$class];
    $exported_objects = $controller->export($object_ids);
    foreach ($exported_objects as $object_id => $object_definition) {
      $transport->store($object_id, $object_definition);
    }
  }
}

function drush_stageable_update() {
  $args = func_get_args();

  if (count($args) < 1) {
    return drush_set_error(dt('Usage: drush stageable-update [modules...].'));
  }

  $modules = $args;
  foreach ($modules as $module) {
    if (!module_exists($module)) {
      return drush_set_error(dt('Module !module does not exists or is not enabled.', array('!module' => $module)));
    }
  }

  module_load_include('inc', 'stageable', 'stageable.export');

  foreach ($modules as $module) {
    stageable_update_entities($module);
  }
}

function drush_stageable_revert() {
  $args = func_get_args();

  if (count($args) < 1) {
    return drush_set_error(dt('Usage: drush stageable-revert [module] [entities...].'));
  }

  $module = array_shift($args);
  if (!module_exists($module)) {
    return drush_set_error(dt('Module !module does not exists or is not enabled.', array('!module' => $module)));
  }

  // Parse the list of entities.
  $entity_info = entity_get_info();
  $entities = array();
  foreach ($args as $entity) {
    if (preg_match('/^([^:]+):([0-9]+)$/', $entity, $matches)) {
      list(, $entity_type, $entity_id) = $matches;
      if (isset($entity_info[$entity_type])) {
        // Valid entity specification.
        $entities[$entity_type][] = $entity_id;
        continue;
      }
    }
    return drush_set_error(dt('Invalid entity specification !entity.', array('!entity' => json_encode($entity))));
  }

  module_load_include('inc', 'stageable', 'stageable.export');

  if (empty($entities)) {
    $entities = stageable_exported_entities($module);
  }

  foreach ($entities as $module) {
    stageable_revert_entities($module, $entity_type, $entity_ids);
  }
}
