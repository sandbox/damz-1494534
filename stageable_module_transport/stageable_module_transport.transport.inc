<?php

class StageableModuleTransport implements StageableTransport {
  public function __construct($module) {
    $this->module = $module;
    $this->modulePath = drupal_get_path('module', $this->module);
  }

  protected function getFilePath($object_path) {
    return $this->modulePath . '/' . str_replace(':', '/', $object_path) . '.json';
  }

  public function fetch($object_path) {
    $filepath = $this->getFilePath($object_path);
    if (is_file($filepath)) {
      return stageable_json_decode_mergeable(file_get_contents($filepath));
    }
  }

  public function store($object_path, $object) {
    $filepath = $this->getFilePath($object_path);
    @mkdir(dirname($filepath), 0777, TRUE);
    file_put_contents($filepath, stageable_json_encode_mergeable($object));
  }

  protected $objectPaths = NULL;
  protected $currentObjectPath = NULL;

  public function current() {
    if (isset($this->currentObjectPath)) {
      return $this->fetch($this->currentObjectPath);
    }
  }

  public function key() {
    return $this->currentObjectPath;
  }

  public function rewind() {
    if (!isset($this->objectPaths)) {
      $this->objectPaths = array();
      $objects = file_scan_directory($this->modulePath, '/\.json$/');
      foreach ($objects as $object) {
        $object_path = substr(substr($object->uri, 0, -5), strlen($this->modulePath) + 1);
        $object_path = str_replace('/', ':', $object_path);
        $this->objectPaths[] = $object_path;
      }
    }
    reset($this->objectPaths);
    $this->next();
  }

  public function next() {
    $each = each($this->objectPaths);
    $this->currentObjectPath = $each ? $each['value'] : NULL;
  }

  public function valid() {
    return isset($this->currentObjectPath);
  }
}

/**
 * Returns a JSON representation of a datastructure optimized for mergeability.
 */
function stageable_json_encode_mergeable($content) {
  $content = _stageable_json_encode_mergeable_flatten($content, '');
  return PrettyJSON::encode($content);
}

function _stageable_json_encode_mergeable_flatten(array $content, $path) {
  $output = array();
  $path = $path !== '' ? $path . ':' : '';
  foreach ($content as $key => $value) {
    $key = str_replace(':', '::', $key);
    if (is_array($value)) {
      $flattened_value = _stageable_json_encode_mergeable_flatten($value, $path . $key);
      $output += $flattened_value;
    }
    else {
      $output[$path . $key] = $value;
    }
  }
  return $output;
}

function stageable_json_decode_mergeable($content) {
  $content = @json_decode($content);
  if ($content === NULL) {
    return;
  }

  $output = array();
  foreach ($content as $key => $value) {
    // ':' literals in keys are escaped as '::' by _json_encode_mergeable_flatten().
    // Protect them from explode().
    $path = explode(':', str_replace('::', "\0", $key));
    if (count($path) > 1) {
      foreach ($path as &$component) {
        $component = str_replace("\0", ':', $component);
      }
      drupal_array_set_nested_value($output, $path, $value);
    }
    else {
      $output[$key] = $value;
    }
  }

  return $output;
}
