

* How to process dependencies between entities?

Option 1: fetch all the dependencies
Option 2: leave the dependencies and process everything one by one
Option 3: be smart


# API

GET object/:objectid
  => Get a particular object.

GET object
  => List all objects

GET object?changed-since=[mark]
  => List all objects that changed since [mark]

GET object?type=[type]
  => List all objects of a particular type prefix

GET object?type=[type]&token=[token]
  => Get objects based on a particular (opaque) token
