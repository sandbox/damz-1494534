<?php

/**
 * Implements hook_services_resources().
 */
function stageable_services_transport_services_resources() {
  $resources['stageable']['index'] = array(
    'file' => array('type' => 'inc', 'module' => 'stageable_services_transport', 'name' => 'stageable_services_transport.services'),
    'callback' => array('StageableServicesController', 'index'),
    'args' => array(
      array(
        'name' => 'type',
        'optional' => TRUE,
        'type' => 'string',
        'description' => 'A type prefix that will filter the objects being returned.',
        'default value' => '',
        'source' => array('param' => 'type'),
      ),
      array(
        'name' => 'token',
        'optional' => TRUE,
        'type' => 'string',
        'description' => 'An opaque token that the sender can use to get more results for a result set.',
        'default value' => NULL,
        'source' => array('param' => 'token'),
      ),
    ),
    'access callback' => '_stageable_access',
  );
  return $resources;
}

function _stageable_access() {
  return TRUE;
}

class StageableServicesController {
  static function index($type = '', $token = NULL) {
    try {
      $query = db_select('stageable_track', 't')
        ->fields('t');

      if ($type !== '') {
        $query->condition('object_id', db_like($type) . ':', 'LIKE');
      }
      $query->range(0, 10);

      $output = array();
      foreach ($query->execute() as $row) {
        $output[] = stageable_get_stageable_object($row->object_id)->export(array($row->object_id));
      }

      return $output;
    }
    catch (Exception $e) {
      services_error(str_replace("\n", " ", (string) $e));
    }
  }
}